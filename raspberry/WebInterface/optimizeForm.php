<!DOCTYPE html>
<html>
    <head>
		<title>Ottimizzazione database</title>
        <meta charset="utf-8" />
		<?php
			//detecting mobile browsers
			require_once 'extensions/detectmobilebrowser.php';
			if (isMobileBrowser() || isset($_GET['mobile'])) {
				$cssFolder = 'css/mobile';
			} else {
				$cssFolder = 'css/desktop';
			}
		?>
		<!-- My CSS -->
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/main.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/optimize.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/title.css" />
		<script language='Javascript'>
			var OPTIMIZE_SCRIPT = 'optimizerScript.php';
			var es;
			var lock = false;		//Lock to only one optimization at time
			  
			function startTask() {
				if (lock) {
					return;
				} else {
					lock = true;
					document.getElementById("btnStart").disabled = true;
				}
				document.getElementById("artDetails").style.display = "block";
				document.getElementById("results").innerHTML = "";
				var yearToOptimize = document.getElementById("numYear").value;
				var targetAddress = OPTIMIZE_SCRIPT + "?year=" + yearToOptimize;
				es = new EventSource(targetAddress <?php if (isset($_GET['sample'])) echo '+ "&sample"'?>);
				document.getElementById("labDetails").innerHTML = "Ottimizzazione in corso...";
				  
				//a message is received
				es.addEventListener('message', function(e) {
					var result = JSON.parse( e.data );
					  
					addLog(e.lastEventId, result.message);       
					  
					if(e.lastEventId == 'CLOSE') {
						//Optimization finished
						// addLog(e.lastEventId, 'Ottimizzazione completata');
						es.close();
						var pBar = document.getElementById('progressor');
						pBar.value = pBar.max; //max out the progress bar
						lock = false;
						document.getElementById("btnStart").disabled = false;
						document.getElementById("labDetails").innerHTML = "Ottimizzazione completata";
					}
					else {
						//Percentage update
						var pBar = document.getElementById('progressor');
						pBar.value = result.progress;
						var perc = document.getElementById('percentage');
						perc.innerHTML   = result.progress  + "%";
						perc.style.width = (Math.floor(pBar.clientWidth * (result.progress/100)) + 15) + 'px';
					}
				});
				  
				es.addEventListener('error', function(e) {
					addLog('ERROR', 'Error occurred');
					document.getElementById("labDetails").innerHTML = "ERRORE: Nessun dato registrato per l'anno scelto?";
					es.close();
					lock = false;
					document.getElementById("btnStart").disabled = false;
				});
			}
			  
			function stopTask() {
				es.close();
				addLog(0, 'Interrupted');
			}
			  
			function addLog(type, message) {
				var r = document.getElementById('results');
				r.innerHTML += message;
				if (type != 'INLINE') {
					r.innerHTML += '<br>';
				}
				r.scrollTop = r.scrollHeight;
			}
			
			
			//Starting script on page load
			// window.onload = startTask();
		</script>
		<script>
			var detailsShown = false;
			function toggleDetails() {
				var display;
				if (detailsShown) {
					display = "none";
				} else {
					display = "block";
				}
				document.getElementById("results").style.display = display;
				detailsShown = !detailsShown;
			}
		</script>
    </head>
    <body>
		<aside>
			<?php include 'menu.inc.html'; ?>
			<hr />
			<?php include 'todayInfo.inc.php'; ?>
		</aside>
		<section id="content">
			<article id="main-title">
				<h1>Ottimizzazione database</h1>
			</article>
			<div id="spacer"></div>
			
			<!-- Form to start a new year optimization -->
			<article>
				<div>Scegli l'anno da ottimizzare: <input type="number" id="numYear" value="<?php echo date("Y"); ?>"></div>
				<div><input id="btnStart" type="button" value="Inizia" onClick="startTask()"></div>
			</article>
			
			<!-- Details of optimization -->
			<article id="artDetails">
				<div><span id="labDetails">Dettagli ottimizzazione</span></div>
				<div>
					<progress id='progressor' value="0" max='100'></progress>  
					<span id="percentage">0%</span>
				</div>
				<input type="button" value="Dettagli" onClick="toggleDetails()">
				<div id="results"></div>
			</article>
		</section>
    </body>
</html>