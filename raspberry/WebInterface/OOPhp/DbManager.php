<?php
    require_once 'DbConnector.php';
	require_once 'DbOptimizer.php';
	require_once 'AdvDateTime.php';

    /**
     * Class used to retrieve values from database.
     * 
     * This class contains methods to retrieve values from database to hide
     * connection and queries.
     * 
     * @author Gioele Masini
     */
    class DbManager 
    {
        const DB_SAMPLE_EDINBURGH = 		DbConnector::DB_SAMPLE_EDINBURGH;
        const DB_ARDUINO = 					DbConnector::DB_ARDUINO;

        const OPTIMIZATION_DAILY = 			DbOptimizer::RES_DAILY;
        const OPTIMIZATION_HOURLY = 		DbOptimizer::RES_HOURLY;
		
		const RESOLUTION_DAY = 				'day';
		const RESOLUTION_MONTH = 			'month';
		const RESOLUTION_YEAR = 			'year';
		const RESOLUTION_LAST = 			'last';
		
		const VALUE_TEMPERATURE = 			'temperature';
		const VALUE_WINDSPEED = 			'windspeed';
		const VALUE_RAINFALL = 				'rainfall';
		const VALUE_BRILLIANCE = 			'brilliance';
		
		private static $errors = '';

		/**
         * Return all requested values from optimization year file.
         * 
         * @param $datetime from which the method will get year.
		 * @param $valueToShow value to retrieve, use one of this class constants starting with "VALUE_"
         * 
         * @return array[] array of arrays with values retrieved. Every row has two values: datetime and value.
         */
		public static function getYearAvgValues($datetime, $valueToShow)
		{
			$year = $datetime->getYear();
			$filename = DbOptimizer::getOptimizationFileName($year, DbManager::OPTIMIZATION_DAILY);
			
			switch($valueToShow) {
				case DbManager::VALUE_TEMPERATURE:	$valueToShowIndex = 1;	break;
				case DbManager::VALUE_WINDSPEED: 	$valueToShowIndex = 2;	break;
				case DbManager::VALUE_RAINFALL: 	$valueToShowIndex = 3;	break;
				case DbManager::VALUE_BRILLIANCE: 	$valueToShowIndex = 4;	break;
				default:							$valueToShowIndex = 1;	break;
			}
			
			$file = file_get_contents($filename);
			$arrayValues = unserialize($file);
			
			$toReturn = [];
			foreach($arrayValues as $row) {
				$temp = [$row[0], $row[$valueToShowIndex]];
				array_push($toReturn, $temp);	//[datetime, temperature]
			}
			return $toReturn;
 		}
		
		/**
         * Return all requested values from optimization month file.
         * 
         * @param $datetime from which the method will get the right month and year.
		 * @param $valueToShow value to retrieve, use one of this class constants starting with "VALUE_"
         * 
         * @return array[] array of arrays with values retrieved. Every row has two values: datetime and value.
         */
		public static function getMonthAvgValues($datetime, $valueToShow)
		{
			$year = $datetime->getYear();
			$month = $datetime->getMonth();
			
			switch($valueToShow) {
				case DbManager::VALUE_TEMPERATURE:	$valueToShowIndex = 1;	break;
				case DbManager::VALUE_WINDSPEED: 	$valueToShowIndex = 2;	break;
				case DbManager::VALUE_RAINFALL: 	$valueToShowIndex = 3;	break;
				case DbManager::VALUE_BRILLIANCE: 	$valueToShowIndex = 4;	break;
				default:							$valueToShowIndex = 1;	break;
			}
			
			$filename = DbOptimizer::getOptimizationFileName($year, DbManager::OPTIMIZATION_HOURLY);
			try {
				$file = file_get_contents($filename);
				$arrayValues = unserialize($file);
			} catch (Exception $e) {
				DbManager::$errors .= "Impossibile recuperare i dati da " . $filename . PHP_EOL;
				$arrayValues = [];
			}
			$toReturn = [];
			// $countValues = $totValues = 0;
			foreach($arrayValues as $row) {
                    $datetime = AdvDateTime::millisecondsToDateTime($row[0]);
				if ($datetime->getMonth() == $month) {
					$temp = [$row[0], $row[$valueToShowIndex]];
					array_push($toReturn, $temp);	//[datetime, temperature]
				}
			}
			return $toReturn;
 		}
		
		/**
         * Return all requested values from database. From $dateStart to $dateStart+1D.
         * 
         * @param $dateStart datetime from retrieved data will start.
		 * @param $valueToShow value to retrieve, use one of this class' constants starting with "VALUE_"
		 * @param $database database to use, use one of this class' constants starting with "DB_"
         * 
         * @return array[] array of arrays with values retrieved from database. Every row has two values: datetime and value to show.
         */
		public static function getDayValues($dateStart, $valueToShow = DbManager::VALUE_TEMPERATURE, $database = DbManager::DB_ARDUINO)
		{
			return DbManager::getValues($valueToShow, $dateStart, DbManager::RESOLUTION_DAY, $database);
		}
        
        public static function getValues($valueName, $dateStart, $resolution, $database = DbManager::DB_ARDUINO, $limit = null)
        {
            $connection = DbConnector::getConnection($database);
			
			//Setting end date for query
			$dateStart->setTime(0, 0, 0);	//Resetting time of datetime input object
			switch($resolution) {
				case DbManager::RESOLUTION_DAY: 
					$dateEnd = clone $dateStart;	
					$dateEnd->add(new DateInterval('P1D'));
					break;
				case DbManager::RESOLUTION_MONTH:
					$dateStart->setDate($dateStart->getYear(), $dateStart->getMonth(), 1);
					$dateEnd = clone $dateStart;	
					$dateEnd->add(new DateInterval('P1M'));
					break;
				case DbManager::RESOLUTION_YEAR:
					$dateStart->setDate($dateStart->getYear(), 1, 1);
					$dateEnd = clone $dateStart;	
					$dateEnd->add(new DateInterval('P1Y'));
					break;
			}
			
            $sql = 'SELECT ';

            if ($valueName && $valueName != 'all') {
                $sql .= 'datetime, ' . $valueName;
            } else {
                $sql .= '*';			// datetime, rainfall, windspeed, temperature, brilliance
            }
            
            $sql .= ' FROM data_' . $dateStart->getYear();
			
			$sql .= ' WHERE datetime >= ' . $dateStart->toMilliseconds();
			$sql .= ' AND 	datetime <= ' . ($dateEnd->toMilliseconds()-1);
			
			$sql .= ' ORDER BY datetime DESC';

            if ($limit) {
                $sql .= ' LIMIT ' . $limit;
            }

            $result = DbManager::executeQuery($sql, $connection);
            
			if (!$result) {
				//Something gone wrong with query.
				//Probably no data for input date
				return null;
			}
			
			//Creo un array con i valori recuperati
            $arrayValues = [];
            while ($row = $result->fetchArray()) {
				if ($valueName && $valueName != 'all') {
					$temp = [$row['datetime']+0, $row[$valueName]];
				} else {
					if (isset($_GET['sample'])) {
						$temp = [$row['datetime']+0, $row['temperature'], $row['windspeed'], $row['rainfall']];
					} else {
						$temp = [$row['datetime']+0, $row['temperature'], $row['windspeed'], $row['rainfall'], $row['brilliance']];
					}
				}
               array_push($arrayValues, $temp);
            }
			$result->finalize();
            DbConnector::releaseConnection($connection);
            return $arrayValues;
        }
		
		/**
         * Return last database values of today.
         * 
		 * @param $database database to use, use one of this class' constants starting with "DB_"
         * 
         * @return int[] array with values retrieved from database. Composed as: [datetime, temperature, windspeed, rainfall, brilliance]
         */
		public static function getLastValuesOfTheDay($database = DbManager::DB_ARDUINO)
		{
			$dateStart = new AdvDateTime();
			$dateStart->setTime(0, 0, 0);
			$dateEnd = clone $dateStart;	
			$dateEnd->add(new DateInterval('P1D'));
			
			$connection = DbConnector::getConnection($database);
			$sql = 'SELECT * FROM data_' . $dateStart->getYear()
				 . ' WHERE datetime >= ' . $dateStart->toMilliseconds()
				 . ' AND datetime < ' . $dateEnd->toMilliseconds()
				 . ' ORDER BY datetime DESC LIMIT 1';
			$result = DbManager::executeQuery($sql, $connection);
			
			if (!$result) {
				//Something gone wrong with query.
				//Probably no data for input date
				$values = NULL;
			} else {
				$row = $result->fetchArray();
				if ($row) {
					if (isset($_GET['sample'])) {
						$values = [$row['datetime']+0, $row['temperature'], $row['windspeed'], $row['rainfall']];
					} else {
						$values = [$row['datetime']+0, $row['temperature'], $row['windspeed'], $row['rainfall'], $row['brilliance']];
					}
				} else {
					$values = NULL;
				}
				$result->finalize();
			}
			
            DbConnector::releaseConnection($connection);
            return $values;
		}
        
        private static function executeQuery($sql, $connection)
        {
            // var_dump($sql);
            return $connection->query($sql);
        }
    }
