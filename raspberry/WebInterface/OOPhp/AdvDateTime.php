<?php
/**
 * Class used to extend DateTime with support of milliseconds conversion and value getters.
 *
 * @author Gioele Masini
 */
class AdvDateTime extends DateTime 
{
	/****** DATE UTILITY FUNCTIONS ******/
	public function hoursAreEquals($dateToCompare)
	{
		$format = 'd/m/Y h';
		return $this->datesAreEquals($dateToCompare, $format);
	}
	
	public function daysAreEquals($dateToCompare)
	{
		$format = 'd/m/Y';
		return $this->datesAreEquals($dateToCompare, $format);
	}
	
	public function monthsAreEquals()
	{
		$format = 'm/Y';
		return $this->datesAreEquals($dateToCompare, $format);
	}
	
	public function datesAreEquals($dateToCompare, $format)
	{
		if ($this->format($format) === $dateToCompare->format($format)) {
			return true;
		}
		return false;
	}
	
	public function toMilliseconds()
	{
		return ((int)$this->format('U'))*1000;
	}
	
	public static function millisecondsToDateTime($dateInMilliseconds)
	{
		date_default_timezone_set('GMT');
		$t = $dateInMilliseconds / 1000;
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		return new AdvDateTime( date('Y-m-d H:i:s.'.$micro, $t) );
	}
	
	public function getDay()
	{
		return (int) $this->format('j');
	}
	
	public function getMonth()
	{
		return (int) $this->format('n');
	}
	
	public function getYear()
	{
		return (int) $this->format('Y');
	}
	
	/** Return hour in 24-hour format **/
	public function getHour()
	{
		return (int) $this->format('G');
	}
	
	public function getMinutes()
	{
		return (int) $this->format('i');
	}
	
	public function getSeconds()
	{
		return (int) $this->format('s');
	}
	
	public static function createFromFormat($format, $stringToConvert)
	{
		$datetime = DateTime::createFromFormat($format, $stringToConvert);
		return new AdvDateTime($datetime->format('Y-m-d H:i:s'));
	}
}