<?php
require_once 'DbConnector.php';
require_once 'AdvDateTime.php';

/**
 * Class used to optimize and cache database's daily data.
 *
 * This class' methods send messages readable from javascript with an asynchronous request.
 *
 * @author Gioele Masini
 */
class DbOptimizer
{
	//Optimization file names
	private static $OPT_FILE_NAME_PATH = 'data/cache/';
	private static $OPT_FILE_NAME_BASE = 'optimization';
	private static $OPT_FILE_NAME_EXT = '.dat';
	private static $OPT_FILE_SEPARATOR_DAILY = 'daily';
	private static $OPT_FILE_SEPARATOR_HOURLY = 'hourly';
	
	//Resolutions available for optimization
	const RES_DAILY = 'daily';
	const RES_HOURLY = 'hourly';
	
	
	public static function prepareOptimization($year)
	{
		//Creating folder 'cache' if not exists
		$dir = DbOptimizer::$OPT_FILE_NAME_PATH;
		is_dir($dir) || @mkdir($dir) || DbOptimizer::send_message(0, "Can't create folder " . $dir, 0);
		
		//Clearing optimization file from older optimizations
		file_put_contents(DbOptimizer::getOptimizationFileName($year, DbOptimizer::RES_DAILY), '', LOCK_EX);
		file_put_contents(DbOptimizer::getOptimizationFileName($year, DbOptimizer::RES_HOURLY), '', LOCK_EX);
	}
	
	public static function optimize($year, $database = DbConnector::DB_ARDUINO)
	{
		set_time_limit(0);			//Disabling timer of max execution script time
		
		DbOptimizer::send_message(0, 'Starting optimization', 0);
		try {
			$conn = DbConnector::getConnection($database);
		} catch (Exception $e) {
			DbOptimizer::send_message(0, 'Connection error ' . $e->getMessage(), 0);
		}

		DbOptimizer::send_message(0, 'Recovering daily data', 0);
		
		/**** Getting all values from db and calculating daily and hourly averages ****/
		$sql = 'SELECT * FROM data_'.$year;
		$rows = $conn->query($sql);
		DbOptimizer::send_message(0, '| ' . $sql , 0);
		DbOptimizer::send_message(0, '| done with code: ' . $conn->lastErrorCode() , 0);
		
		$oldDateObj = $oldDateMs = NULL;
		$valuesCount = $valuesTotal = $valuesAvg = [];
		
		$valuesCount['hour'] = $valuesCount['day'] = 0;
		$valuesAvg['hour'] = $valuesAvg['day'] = [];
		$valuesTotal['hour'] = $valuesTotal['day'] = [];
		
		$valuesTotal['hour']['temp'] = $valuesTotal['hour']['wind'] = $valuesTotal['hour']['rain'] = $valuesTotal['hour']['light'] =
			$valuesTotal['day']['temp'] = $valuesTotal['day']['wind'] = $valuesTotal['day']['rain'] = $valuesTotal['day']['light'] = 0;
		
		DbOptimizer::send_message(0, 'Calculating averages' , 0);
		$nextRow=-9327;
		if($row = $rows->fetchArray()){
			while ($row) {
				if ($nextRow!=-9327)	$row=$nextRow;
				$nextRow = $rows->fetchArray();
			
				$dateMs = $row['datetime'];
				$dateObj = AdvDateTime::millisecondsToDateTime($dateMs);
				if (!$nextRow||($oldDateObj !== NULL && !$dateObj->hoursAreEquals($oldDateObj))) {
					//Hour changed, saving hourly avg data
					$dateAverageObj = clone $oldDateObj;
					$hour = $dateAverageObj->getHour();
					$dateAverageObj->setTime($hour,0,0);
					$dateAverageMs = $dateAverageObj->toMilliseconds();
					
					$avgTemperature = round($valuesTotal['hour']['temp'] / $valuesCount['hour'], 2);
					$avgWindspeed = round($valuesTotal['hour']['wind'] / $valuesCount['hour'], 2);
					$avgRainfall = round($valuesTotal['hour']['rain'], 2);
					if (!isset($_GET['sample'])) {
						$avgBrilliance = round($valuesTotal['hour']['light'] / $valuesCount['hour'], 2);
						$avgdata = [$dateAverageMs, $avgTemperature, $avgWindspeed, $avgRainfall, $avgBrilliance];
					} else {
						$avgdata = [$dateAverageMs, $avgTemperature, $avgWindspeed, $avgRainfall];
					}
					
					array_push($valuesAvg['hour'], $avgdata);
				
					$valuesCount['hour'] = 
						$valuesTotal['hour']['temp'] = $valuesTotal['hour']['wind'] = $valuesTotal['hour']['rain'] = $valuesTotal['hour']['light'] = 0;
				}
				if (!$nextRow||($oldDateObj !== NULL && !$dateObj->daysAreEquals($oldDateObj))) {
					//Day changed, saving daily avg data
					$dateAverageObj = clone $oldDateObj;
					$dateAverageObj->setTime(0,0,0);
					$dateAverageMs = $dateAverageObj->toMilliseconds();
					
					$avgTemperature = round($valuesTotal['day']['temp'] / $valuesCount['day'], 2);
					$avgWindspeed = round($valuesTotal['day']['wind'] / $valuesCount['day'], 2);
					$avgRainfall = round($valuesTotal['day']['rain'], 2);
					if (!isset($_GET['sample'])) {
						$avgBrilliance = round($valuesTotal['day']['light'] / $valuesCount['day'], 2);
						$avgdata = [$dateAverageMs, $avgTemperature, $avgWindspeed, $avgRainfall, $avgBrilliance];
					} else {
						$avgdata = [$dateAverageMs, $avgTemperature, $avgWindspeed, $avgRainfall];
					}
					array_push($valuesAvg['day'], $avgdata);
				
					$valuesCount['day'] = 
						$valuesTotal['day']['temp'] = $valuesTotal['day']['wind'] = $valuesTotal['day']['rain'] = $valuesTotal['day']['light'] = 0;
					
					DbOptimizer::send_message('INLINE', '.' , round(100 * sizeof($valuesAvg['day']) / 365));
				}
				
				$valuesTotal['hour']['temp'] += $row['temperature'];
				$valuesTotal['hour']['wind'] += $row['windspeed'];
				$valuesTotal['hour']['rain'] += $row['rainfall'];
				$valuesCount['hour']++;
				
				$valuesTotal['day']['temp'] += $row['temperature'];
				$valuesTotal['day']['wind'] += $row['windspeed'];
				$valuesTotal['day']['rain'] += $row['rainfall'];
				$valuesCount['day']++;
								
				if (!isset($_GET['sample'])) {
					$valuesTotal['hour']['light'] += $row['brilliance'];
					$valuesTotal['day']['light'] += $row['brilliance'];
				}
				
				$oldDateObj = $dateObj;
				$oldDateMs = $dateMs;
			}
		}
		
		DbOptimizer::send_message(0, '' , 100);
		DbOptimizer::send_message(0, '| done, calculated values: ' . sizeof($valuesAvg['day']) . ' daily, ' . sizeof($valuesAvg['hour']) . ' hourly', 100);
		
		DbOptimizer::send_message(0, 'Creating optimization files' , 100);
		
		//Saving values on two different files
		DbOptimizer::prepareOptimization($year); 		//Clearing old optimization files
		
		$fileNameDaily = DbOptimizer::getOptimizationFileName($year, DbOptimizer::RES_DAILY);
		$fileNameHourly = DbOptimizer::getOptimizationFileName($year, DbOptimizer::RES_HOURLY);
		file_put_contents($fileNameDaily, serialize($valuesAvg['day']), LOCK_EX);
		DbOptimizer::send_message(0, '| ' . $fileNameDaily, 100);
		file_put_contents($fileNameHourly, serialize($valuesAvg['hour']), LOCK_EX);
		DbOptimizer::send_message(0, '| ' . $fileNameHourly, 100);
			
		DbOptimizer::send_message(0, '| done' , 100);
		
		$rows->finalize();
		DbConnector::releaseConnection($conn);
	}
	
	public function sendCloseMessage()
	{
		DbOptimizer::send_message('CLOSE', 'Process complete', 100);
	}
	
	public static function send_message($id, $message, $progress)
	{
		//Tipologie di messaggi inviabili: 0 (normal), CLOSE (last one) INLINE (without newline), SUBSTITUTE (in place of last one)
		$toReturn = array('message' => $message , 'progress' => $progress);
		  
		echo "id: $id" . PHP_EOL;
		echo "data: " . json_encode($toReturn) . PHP_EOL;
		echo PHP_EOL;
		  
		ob_flush();
		flush();
	}
	
	public static function getOptimizationFileName($year, $resolution)
	{			
		if ($resolution == DbOptimizer::RES_DAILY || $resolution == DbOptimizer::RES_HOURLY) {
			return 
				DbOptimizer::$OPT_FILE_NAME_PATH
				. DbOptimizer::$OPT_FILE_NAME_BASE
				. '-' . $year 
				. '-' . $resolution
				. (isset($_GET['sample']) ? '-sample' : '')
				. DbOptimizer::$OPT_FILE_NAME_EXT;
		}
		return null;
	}
}	
