<?php
    /**
     * Class used to connect to database.
     * 
     * This class contains methods to get or release connections.
     *
     * @author Gioele Masini
     */
    class DbConnector
    {
		const DB_SAMPLE_EDINBURGH = 'data/sample.db';
		const DB_ARDUINO = 'data/arduino.db';
		
		// private static $PAGE_OPTIMIZEDB = 'optimizeDb.php';
				
        /**
         * Retrieve a connection to execute queries.
         * 
         * Get a connection, if available, to mysql database.
         * 
         * @throws Exception if it is not possible to return a valid connection.
         * 
         * @return mysql_connection return db connection with default database selected.
         */
        public static function getConnection($database)
        {
			return new SQLite3($database);
        }
        
        /**
         * Release a retrieved connection.
         * 
         * @param mysql_connection $connection connection to be released.
         * 
         * @return void
         */
        public static function releaseConnection($connection)
        {
            if ($connection) {
                $connection->close();
            }
        }
		
		public static function getLastDbError()
		{
			return SQLite3::lastErrorMsg();
		}
    }
