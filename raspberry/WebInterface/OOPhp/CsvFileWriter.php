<?php
    /**
     * Class used to write data on csv file.
     *
     * This class contains methods to write input data on input files in csv format.
     *
     * @author Gioele Masini
     */
    class CsvFileWriter
    {
        /**
         * Write an array of data.
         * 
         * 
         * @param resource $file output file which will contains wrote data.
         * @param string[] $headers array of strings, contains columns' headers, nullable.
         * @param array[] $rowsOfData array of array of objects, contains data to write on file.
         * 
         * @return void
         */
        public static function writeArray($file, $headers, $rowsOfData)
        {
            // writing headers, if specified
            if ($headers) {
                fputcsv($file, $headers);
            }
            
			if ($rowsOfData) {
				// loop over the rows, outputting them
				foreach ($rowsOfData as $row) {
					fputcsv($file, $row);
				}
			}
        }
    }
