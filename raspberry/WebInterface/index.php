<html>
	<head>
		<title>Stazione metereologica</title>
		<?php 
			//detecting mobile browsers
			require_once 'extensions/detectmobilebrowser.php';
			if (isMobileBrowser() || isset($_GET['mobile'])) {
				$cssFolder = 'css/mobile';
			} else {
				$cssFolder = 'css/desktop';
			}
		?>
		
		<!-- My CSS -->
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder; ?>/main.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder; ?>/index.css" />
	</head>
	<body>
		<aside>
			<?php include 'menu.inc.html'; ?>
			<hr />
			<?php include 'todayInfo.inc.php'; ?>
		</aside>
		<section>
			<article id="title">
				<div>
					<span><h1>Stazione meteorologica</h1></span>
					<span><h5>made by Gioele Masini</h5></span>
					<span><h5>powered by Raspberry Pi and Arduino</h5></span>
				</div>
			</article>
		</section>
</body></html>
