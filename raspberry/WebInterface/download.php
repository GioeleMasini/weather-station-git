<?php
    require_once 'OOPhp/DbManager.php';
    require_once 'OOPhp/CsvFileWriter.php';
	require_once 'OOPhp/AdvDateTime.php';

    // Script used to download csv file with data requested via GET parameters
	if (isset($_GET['date']) && isset($_GET['resolution']) && isset($_GET['value'])) {
		$date = AdvDateTime::createFromFormat('d/m/Y', $_GET['date']);
		$valueToWrite = $_GET['value'];
		$resolution = $_GET['resolution'];
		
		if (isset($_GET['sample'])) {
			$database = DbManager::DB_SAMPLE_EDINBURGH;
		} else {
			$database = DbManager::DB_ARDUINO;
		}
		
		$fileName = $valueToWrite . '_' . $resolution . '_' . $date->format('d-m-Y');

		
		//Creating header
		if ($valueToWrite == 'all') {
			if (isset($_GET['sample'])) {
				$header = ['datetime', 'temperature', 'windspeed', 'rainfall'];
			} else {
				$header = ['datetime', 'temperature', 'windspeed', 'rainfall', 'brilliance'];
			}
		} else {
			$header = ['datetime', $valueToWrite];
		}
		
		// changing maximum php memory limit
		ini_set("memory_limit","512M");
		
		// retrieve values
		$rows = DbManager::getValues($valueToWrite, $date, $resolution, $database);

		if (isset($_GET['raw'])) {
			header("Content-Type: application/json; charset=UTF-8");
			$output = '{"headers": ' . json_encode($header) . ', "data": ' . json_encode($rows) . '}';
			echo $output;
		} else {
			//CSV
			// changing output headers so that the file is downloaded rather than displayed
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=' . $fileName . '.csv');

			// create a file pointer connected to the output stream
			$output = fopen('php://output', 'w');
			// creating CSV file
			CsvFileWriter::writeArray($output, $header, $rows);
			
			fclose($output);
		}
	}