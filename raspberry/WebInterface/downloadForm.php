<!DOCTYPE html>
<html>
    <head>
		<title>Download dati</title>
		<?php
			//detecting mobile browsers
			require_once 'extensions/detectmobilebrowser.php';
			if (isMobileBrowser() || isset($_GET['mobile'])) {
				$cssFolder = 'css/mobile';
			} else {
				$cssFolder = 'css/desktop';
			}
		?>
		<!-- My CSS -->
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/main.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/title.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/optimize.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder ?>/download.css" />
		
		<!-- datepicker Widget code -->
		<link rel="stylesheet" media="screen" type="text/css" href="extensions/datepicker/css/datepicker.css" />
		<script type="text/javascript" src="extensions/datepicker/js/jquery.js"></script>
		<script type="text/javascript" src="extensions/datepicker/js/datepicker.js"></script>
		<!-- Inizializzazione widget -->
		<script type="text/javascript">
		$().ready(function() {
			var dateStart = new Date();
			var options = {
				flat: true,
				format: 'd/m/Y',
				date: dateStart,
				current: dateStart,
				locale: {
					days: ["Domenica", "Luned�", "Marted�", "Mercoled�", "Gioved�", "Venerd�", "Sabato", "Domenica"],
					daysShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom"],
					daysMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa", "Do"],
					months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
					monthsShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
					weekMin: 'S'
				},
				onChange: function(date) {
					$('#txtDate').val(date);
				}
			};
			$('#datepickerWidget').DatePicker(options);
		});
		</script>
		<script type="text/javascript">
			function startDownload() {
				var downloadPage = 'download.php';
				window.location.href = downloadPage;
			}
		</script>
		
    </head>
    <body>
		<aside>
			<?php include 'menu.inc.html'; ?>
			<hr />
			<?php include 'todayInfo.inc.php'; ?>
		</aside>
		<section id="content">
			<article id="main-title">
				<h1>Download dati</h1>
			</article>
			<div id="spacer"></div>
			
			<!-- Form to start a download -->
			<article>
				<form name="frmDati" action="download.php" method="GET">
					<div>
						<span class="subtitle">Valore:</span>
						<select name="value" id="selValue">
							<option value="all">Tutti</option>
							<option value="temperature">Temperatura</option>
							<option value="windspeed">Vento</option>
							<option value="rainfall">Pioggia</option>
							<?php if (!isset($_GET['sample'])) { ?>
								<option value="brilliance">Luce</option>
							<?php } ?> 
						</select>
					</div>
					<div>
						<span class="subtitle">Risoluzione:</span>
						<select name="resolution" id="selResolution">
						  <option value="year">Anno</option>
						  <option value="month">Mese</option>
						  <option value="day">Giorno</option>
						</select>
					</div>
					<div>
						<div class="bottom-margin">
							<span class="subtitle">Data:</span>
							<input type="text" name="date" value="<?php echo date('d/m/Y'); ?>" id="txtDate" readonly>
						</div>
						<div id="datepickerWidget"></div>
					</div>
					<div>
						<span>Dati d'esempio</span>
						<input type="checkbox" name="sample">
					</div>
					<div><input id="btnDownload" type="submit" value="Download"></div>
				</form>
			</article>
		</section>         
    </body>
</html>