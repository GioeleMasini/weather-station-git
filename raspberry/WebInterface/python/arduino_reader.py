#!/usr/bin/python
import serial
import datetime
import time
import sqlite3
import threading
import os
import json
import traceback
#import logging

DB_PATH = '../data/arduino.db'
TIME_SLEEP = 50				#Time to sleep between every read cycle, in seconds

SERIAL_PORT_NAME = '/dev/ttyACM0'
#LOG_FILE_NAME = 'reader.log'

DATETIME = 'datetime'

# settings
#logging.basicConfig(filename=LOG_FILE_NAME, filemode='w', level=logging.DEBUG)
current_millis = lambda: int(round(time.time() * 1000))

ser = serial.Serial(SERIAL_PORT_NAME, 9600)

def readAndStore():
    conn = None;
    #logging.debug('waiting for data on serial')
    try :
        line = ser.readline()
        #logging.debug('data read: ' + line)
        jsonObj = json.loads(line)
        currentYear = datetime.datetime.now().year
        currentMs = current_millis()

        # opening database connection
        conn = sqlite3.connect('%s' % DB_PATH)
        # creating cursor object to execute queries
        cur = conn.cursor()

        # creating table, if not exists
        #sql = "CREATE TABLE IF NOT EXISTS data_%s (datetime INTEGER, rainfall REAL, windspeed REAL, temperature REAL, brilliance REAL);" % (currentYear)
        sql = "CREATE TABLE IF NOT EXISTS data_%s (%s INTEGER" & (currentYear, DATETIME)
        for key in jsonObj.keys():
            sql += ", %s REAL" % (key)
        sql += ");"
        cur.execute(sql)
        #logging.debug(sql)

        # creating and executing query
        sql = "INSERT INTO data_%s VALUES ( '%s'" % (currentYear, currentMs)
        for value in jsonObj.values():
            sql += ", %s" % (value)
        sql += ");"
        cur.execute(sql)
        conn.commit()
        #logging.debug(sql)

    except Exception as ex:
        print("Exception: ", ex)
        print(traceback.format_exc())
        print("---------------------")
        pass
    
    # closing connection and waiting for next data
    if (conn != None) :
        try :
            #logging.debug("Closing connection")
            conn.close()
        except:
            pass
    
    #Starting a new kernel thread for next read, delayed of TIME_SLEEP seconds
    threading.Timer(TIME_SLEEP, readAndStore).start()

def startLoop():
    PID = os.getpid()
    print(PID)
    ser.readline()	## "spreco" la prima lettura in quanto risulta spesso errata
    readAndStore()


#MAIN
startLoop()