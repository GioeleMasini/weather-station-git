<div id="todayInfo">
		<?php 
			require_once 'OOPhp/DbManager.php';
			require_once 'OOPhp/AdvDateTime.php';
			
			if (isset($_GET['sample'])) {
				$database = DbManager::DB_SAMPLE_EDINBURGH;
			} else {
				$database = DbManager::DB_ARDUINO;
			}
			
			$values = DbManager::getLastValuesOfTheDay($database);
			//$row['datetime'], $row['temperature'], $row['windspeed'], $row['rainfall'], $row['brilliance']
			
			if ($values) {
		?>
			<div><h3>Adesso: </h3></div>
			<div>
				<div class="data"><span class="subtitle">Temperatura:	</span> <?php echo round($values[1], 2) ?> &#176;C	</div>
				<div class="data"><span class="subtitle">Vento:			</span> <?php echo round($values[2], 2) ?> km/h		</div>
				<div class="data"><span class="subtitle">Pioggia:		</span> <?php echo round($values[3], 2) ?> mm			</div>
				<?php if (!isset($_GET['sample'])) { ?>
					<div class="data"><span class="subtitle">Luce:		</span> <?php echo round($values[4], 2) ?> %			</div>
				<?php } ?>
			</div>
		<?php } else { ?>
			<div>Nessun valore registrato oggi!</div>
		<?php } ?>
</div>