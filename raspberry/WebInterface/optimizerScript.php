<?php
require_once 'OOPhp/DbOptimizer.php';

if (isset($_GET['year']))
	$yearToOptimize = $_GET['year'] + 0;
	
	//Checking if using sample data
	if (isset($_GET['sample'])) {
		$database = DbConnector::DB_SAMPLE_EDINBURGH;
	} else {
		$database = DbConnector::DB_ARDUINO;
	}

	// Start of data streaming
	header('Content-Type: text/event-stream');
	// recommended to prevent caching of event data.
	header('Cache-Control: no-cache'); 

	$timeinit = microtime(true);
	try {
	DbOptimizer::optimize($yearToOptimize, $database);
	} catch (Exception $e) {
				DbOptimizer::send_message('0', 'trycatch PHP error: '.error_get_last()['message'], 0);
	}
	$timeend = microtime(true);

	//Closing messages
	DbOptimizer::send_message('0', '___________________________________________', 100);
	DbOptimizer::send_message('0', 'Seconds occurred: '.($timeend - $timeinit), 100);
	$phperror = error_get_last()['message'];
	$phperror = $phperror? $phperror : 'none';
	DbOptimizer::send_message('0', 'PHP error: '.$phperror, 100);
	
	DbOptimizer::sendCloseMessage();
