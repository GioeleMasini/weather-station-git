<html>
	<head>
		<title>Test Grafici</title>
		<?php
			//detecting mobile browsers
			require_once 'extensions/detectmobilebrowser.php';
			if (isMobileBrowser() || isset($_GET['mobile'])) {
				$cssFolder = 'css/mobile';
				$charts_width = '100%';
				$charts_height = '50%';
			} else {
				$cssFolder = 'css/desktop';
				$charts_width = '900px';
				$charts_height = '500px';
			}
		?>
		<?php
			require_once 'OOPhp/DbManager.php';
			require_once 'OOPhp/AdvDateTime.php';
			
			//Checking if using sample data
			if (isset($_GET['sample'])) {
				$database = DbManager::DB_SAMPLE_EDINBURGH;
			} else {
				$database = DbManager::DB_ARDUINO;
			}
			
			//Recupero i dati da visualizzare
			if (isset($_GET['selValue'])) {
				$valueNameToShow = $_GET['selValue'];
			} else {
				$valueNameToShow = 'temperature';
			}
			
			if (isset($_GET['selResolution'])) {
				$valueResolutionToShow = $_GET['selResolution'];
			} else {
				$valueResolutionToShow = '';
			}
		
			if (isset($_GET['txtDate']) && $_GET['txtDate'] != '') {
				$datetimeToShowString = $_GET['txtDate'];
				$datetimeToShow = AdvDateTime::createFromFormat('d/m/Y', $datetimeToShowString);
			} else {
				// Default value
				$datetimeToShow = new AdvDateTime();											// today
				// $datetimeToShow = AdvDateTime::createFromFormat('d/m/Y', '01/01/2014');		// or custom day
			}
			$datetimeToShow->setTime(0,0,0);
			
			$year = $datetimeToShow->getYear();
			$month = $datetimeToShow->getMonth();
			
			// Selecting correct DbManager value constant
			switch ($valueNameToShow) {
				case 'temperature': 	$valueToRetrieve = DbManager::VALUE_TEMPERATURE; 	break;
				case 'windspeed': 		$valueToRetrieve = DbManager::VALUE_WINDSPEED; 		break;
				case 'rainfall': 		$valueToRetrieve = DbManager::VALUE_RAINFALL; 		break;
				case 'brilliance': 		$valueToRetrieve = isset($_GET['sample']) ? DbManager::VALUE_TEMPERATURE : DbManager::VALUE_BRILLIANCE;
					break;
				default: 				$valueToRetrieve = DbManager::VALUE_TEMPERATURE;	break;
			}
			
			$valuesYear = $valuesMonth = $valuesDay = null;
			switch ($valueResolutionToShow) {
				case 'day': 	$valuesDay = 	DbManager::getDayValues($datetimeToShow, $valueToRetrieve, $database); 	break;
				case 'month': 	$valuesMonth = 	DbManager::getMonthAvgValues($datetimeToShow, $valueToRetrieve); 	break;
				case 'year': 	$valuesYear = 	DbManager::getYearAvgValues($datetimeToShow, $valueToRetrieve); 	break;
				default:
					$valuesDay = 	DbManager::getDayValues($datetimeToShow, $valueToRetrieve, $database);
					$valuesMonth = 	DbManager::getMonthAvgValues($datetimeToShow, $valueToRetrieve);
					$valuesYear = 	DbManager::getYearAvgValues($datetimeToShow, $valueToRetrieve);
					break;
			}
		?>
		
		<!-- datepicker Widget code -->
		<link rel="stylesheet" media="screen" type="text/css" href="extensions/datepicker/css/datepicker.css" />
		<script type="text/javascript" src="extensions/datepicker/js/jquery.js"></script>
		<script type="text/javascript" src="extensions/datepicker/js/datepicker.js"></script>
		<!-- Inizializzazione widget -->
		<script type="text/javascript">
		$().ready(function() {
			var dateStart = new Date(<?php echo $datetimeToShow->toMilliseconds() ?>);
			var options = {
				flat: true,
				format: 'd/m/Y',
				date: dateStart,
				current: dateStart,
				locale: {
					days: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"],
					daysShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom"],
					daysMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa", "Do"],
					months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
					monthsShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
					weekMin: 'S'
				},
				onChange: function(date) {
					$('#txtDate').val(date);
				}
			};
			$('#datepickerWidget').DatePicker(options);
		});
		</script>
		
		<!-- Inserimento valori ottenuti via GET -->
		<script type="text/javascript">
		$().ready(function() {
			$('#selValue').val( '<?php echo $valueNameToShow ?>' );
			$('#selResolution').val( '<?php echo $valueResolutionToShow ?>' );
			$('#txtDate').val('<?php echo $datetimeToShow->format('d/m/Y') ?>');
			
		});
		</script>
		
		<!-- Graph code -->
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		
		<!-- Year chart -->
		<?php if ($valuesYear) { ?>
			<script type="text/javascript">
			  google.load("visualization", "1", {packages:["corechart"]});
			  google.setOnLoadCallback(drawChart);
			  function drawChart() {
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('datetime', 'Data');
				dataTable.addColumn('number', '<?php echo $year ?>');
				
				dataTable.addRows([
					<?php
						$format = 'U';  //Ritorna i millisecondi
						for ($i = 0; $i < sizeof($valuesYear); $i++) {
							$value = $valuesYear[$i];
							$datetime = $value[0];
							$datetime = AdvDateTime::millisecondsToDateTime($datetime);
							if ($i != 0) {
								echo ', ';
							}
							echo '[new Date(' . $datetime->toMilliseconds() . ' ), ' . $value[1] . ']';
						}
					?>
				]);
				
				var dataView = new google.visualization.DataView(dataTable);

				var options = {
				  title: 'Dati annuali',
				  hAxis: {
					title: 'Year',  titleTextStyle: {color: '#333'},
					format: 'MMM yyyy',
					ticks: [
						<?php
						for ($m = 0; $m < 12; $m++) {
							echo 'new Date(' . $year . ',' . $m . ',1)';
							echo $m != 11 ? ',' : '';
						}
						?>
					]
				  },
				  vAxis: {minValue: 0},
				  
				  // Display crosshairs on focus and selection.
				  crosshair: { trigger: 'focus', opacity: 0.5 },
				  
				  //Disegna i punti lungo la linea del grafico
				  pointSize: 2
				  
				};

				var chart = new google.visualization.AreaChart(document.getElementById('chart_year'));
				chart.draw(dataView, options);
			  }
			</script>
		<?php } ?>
		
		<!-- Month chart -->
		<?php if ($valuesMonth) { ?>
			<script type="text/javascript">
			  google.load("visualization", "1", {packages:["corechart"]});
			  google.setOnLoadCallback(drawChart);
			  function drawChart() {
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('datetime', 'Data');
				dataTable.addColumn('number', '<?php echo $datetimeToShow->format('m/Y') ?>');
				
				dataTable.addRows([
					<?php
						$format = 'U';  //Ritorna i millisecondi
						for ($i = 0; $i < sizeof($valuesMonth); $i++) {
							$value = $valuesMonth[$i];
							$datetime = $value[0];
							$datetime = AdvDateTime::millisecondsToDateTime($datetime);
							if ($i != 0) {
								echo ', ';
							}
							echo '[new Date(' . $datetime->toMilliseconds() . ' ), ' . $value[1] . ']';
						}
						$lastDatetime = $datetime;
					?>
				]);
				
				var dataView = new google.visualization.DataView(dataTable);

				var options = {
				  title: 'Dati mensili',
				  hAxis: {
					title: 'Day',  titleTextStyle: {color: '#333'},
					ticks: [
						<?php
						// for ($month = 0; $month < 12; $month++) {
							$d = 1;
							echo 'new Date(' . $year . ',' . ($month-1) . ','. $d . ')';
							for ($d = 5; $d <= $lastDatetime->getDay(); $d+=5) {
								echo ',';
								echo 'new Date(' . $year . ',' . ($month-1) . ','. $d . ')';
							}
						// }
						?>
					]
				  },
				  vAxis: {minValue: 0},
				  
				  // Display crosshairs on focus and selection.
				  crosshair: { trigger: 'focus', opacity: 0.5 },
				  
				  //Disegna i punti lungo la linea del grafico
				  pointSize: 2
				  
				};

				var chart = new google.visualization.AreaChart(document.getElementById('chart_month'));
				chart.draw(dataView, options);
			  }
			</script>
		<?php } ?>
		
		<!-- Day chart -->
		<?php if ($valuesDay) { ?>
			<script type="text/javascript">
				google.load("visualization", "1", {packages:["corechart"]});
				google.setOnLoadCallback(drawChart);
				function drawChart() {
					var dataTable = new google.visualization.DataTable();
					dataTable.addColumn('datetime', 'Data');
					dataTable.addColumn('number', '<?php echo $datetimeToShow->format('d/m/Y') ?>');
					
					dataTable.addRows([
						<?php
							$format = 'Y, n-1, j, G, i, s';  //Ritorna i millisecondi
							for ($i = 0; $i < sizeof($valuesDay); $i++) {
								$value = $valuesDay[$i];
								$datetime = $value[0];
								$datetime = AdvDateTime::millisecondsToDateTime($datetime);
								//echo '/*' . $datetime->format('Y-m-d G:') . '*/';
								if ($i != 0) {
									echo ', ';
								}
								echo '[new Date(' . $datetime->toMilliseconds() . ' ), ' . $value[1] . ']';
							}
							$lastDatetime = $datetime;
						?>
					]);
					var dataView = new google.visualization.DataView(dataTable);

					var options = {
					  title: 'Dati giornalieri',
					  hAxis: {
						title: 'Hour',  titleTextStyle: {color: '#333'},
						ticks: [
							<?php
								$datetimeTemp = clone $datetimeToShow;
								for ($i = 0; $i <= 4; $i++) {
									echo 'new Date(' . $datetimeTemp->toMilliseconds() . '),';
									$datetimeTemp->add(new DateInterval('PT6H'));
								}
							?>
						]
					  },
					  vAxis: {minValue: 0},
					  
					  // Display crosshairs on focus and selection.
					  crosshair: { trigger: 'focus', opacity: 0.5 },
					  
					  //Disegna i punti lungo la linea del grafico
					  pointSize: 2
					  
					};
					var UTCFormatter = new google.visualization.DateFormat({pattern: "HH:mm:ss" });
					UTCFormatter.format(dataTable, 0);

					var chart = new google.visualization.AreaChart(document.getElementById('chart_day'));
					chart.draw(dataView, options);
				}
			</script>
		<?php } ?>
		
		<!-- My CSS -->
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder; ?>/main.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder; ?>/data.css" />
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $cssFolder; ?>/title.css" />
	</head>
	<body>
		<aside>
			<?php include 'menu.inc.html'; ?>
			<hr />
			<form id="datepicker" name="frmDati" action="dataCharts.php" method="GET">
					<div>
						<span class="subtitle">Valore:</span>
						<select name="selValue" id="selValue">
						  <option value="temperature">Temperatura</option>
						  <option value="windspeed">Vento</option>
						  <option value="rainfall">Pioggia</option>
						  <?php if (!isset($_GET['sample'])) { ?>
							<option value="brilliance">Luce</option>
						  <?php } ?>
						</select>
					</div>
					<div>
						<span class="subtitle">Risoluzione:</span>
						<select name="selResolution" id="selResolution">
						  <option value=""></option>
						  <option value="day">Giorno</option>
						  <option value="month">Mese</option>
						  <option value="year">Anno</option>
						</select>
					</div>
					<div class="bottom-margin">
						<div>
							<span class="subtitle">Data:</span>
							<input type="text" name="txtDate" id="txtDate" readonly>
						</div>
						<div id="datepickerWidget"></div>
					</div>
					<?php if (isset($_GET['sample']))	echo '<input type="hidden" value="" name="sample">' ?>
					<div><input type="submit" value="Vai!" id="btnSubmit"></div>
			</form>
			<hr />
			<?php include 'todayInfo.inc.php'; ?>
		</aside>
		<section id="content">
			<article id="main-title">
				<h1>
					<?php
					switch($valueNameToShow) {
						case 'temperature': 	$valueNameIta = 'temperatura (&#176;C)'; 		break;
						case 'windspeed': 		$valueNameIta = 'velocità del vento (km/h)'; 	break;
						case 'rainfall': 		$valueNameIta = 'pioggia (mm)'; 				break;
						case 'brilliance': 		$valueNameIta = isset($_GET['sample']) ? 'temperatura (&#176;C)' : 'luce (%)';	break;
						default: 				$valueNameIta = 'temperatura (&#176;C)';		break;
					}
					echo ucfirst($valueNameIta) 
					?>
				</h1>
			</article>
			<div id="spacer"></div>
			<?php if (!$valueResolutionToShow || $valueResolutionToShow == 'day') { ?>
				<article class="chart">
					<?php if ($valuesDay) { ?>
						<!-- <?php echo 'Recuperati ' . sizeof($valuesDay) . ' valori.'; ?> -->
						<div id="chart_day" style="width: <?php echo $charts_width ?>; height: <?php echo $charts_height ?>;"></div>
					<?php } else { ?>
						<div class="error">Impossibile recuperare i dati giornalieri. Nessun valore registrato per la data scelta.</div>
					<?php } ?>
				</article>
			<?php } ?>
			<?php if (!$valueResolutionToShow || $valueResolutionToShow == 'month') { ?>
				<article class="chart">
					<?php if ($valuesMonth) { ?>
						<!-- <?php echo 'Recuperati ' . sizeof($valuesMonth) . ' valori.'; ?> -->
						<div id="chart_month" style="width: <?php echo $charts_width ?>; height: <?php echo $charts_height ?>;"></div>
					<?php } else { ?>
						<div class="error">Impossibile recuperare i dati mensili. E' necessario prima effettuare l'ottimizzazione del database.</div>
					<?php } ?>
				</article>
			<?php } ?>
			<?php if (!$valueResolutionToShow || $valueResolutionToShow == 'year') { ?>
				<article class="chart">
					<?php if ($valuesYear) { ?>
						<!-- <?php echo 'Recuperati ' . sizeof($valuesYear) . ' valori.'; ?> -->
						<div id="chart_year" style="width: <?php echo $charts_width ?>; height: <?php echo $charts_height ?>;"></div>
					<?php } else { ?>
						<div class="error">Impossibile recuperare i dati annuali. E' necessario prima effettuare l'ottimizzazione del database.</div>
					<?php } ?>
				</article>
			<?php } ?>
		</section>
		<!--
			<?php echo DbManager::$lastError;?><br>
			<?php echo $datetimeToShow->format('d-m-Y H:i:s');?>
		-->
	</body>
</html>
 
