#ifndef __RAINGAUGE__
#define __RAINGAUGE__

#include "Sensor.h"

class RainGauge : public Sensor
{
    unsigned long lastReadTime;
    volatile static int numClocks;
    volatile static unsigned long lastGoodInterrupt;
    
public:
    RainGauge(int pin, int interruptPin);

    String toString(); //override
    String getValueName(); //override
    
    static int getValueType();  //override
  
protected:
    void initialize(int pin, int interruptPin); //override
    
    double makeRead(); //override
    
private:
    static void incClocks();    //Ho dovuto rendere la funzione statica perchè fosse possibile richiamarla con attachInterrupt()
};

#endif

 
