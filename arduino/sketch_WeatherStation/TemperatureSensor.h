#ifndef __TEMPSENSOR__
#define __TEMPSENSOR__

#include "Sensor.h"

class TemperatureSensor : public Sensor
{
    int pin, readCycles;
    
public:
    TemperatureSensor(int pin);
    TemperatureSensor(int pin, int readCycles);

    String toString(); //override
	String getValueName(); //override

protected:
    void initialize(int pin); //override
    
    double makeRead(); //override
    
};

#endif 
