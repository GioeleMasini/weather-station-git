#include "Anemometer.h"

#include <Arduino.h>

volatile unsigned int Anemometer::numRevs = 0;
volatile unsigned long Anemometer::lastGoodInterrupt = 0;

//Pin must be one of 2 and 3 for arduino uno
Anemometer::Anemometer(int pin, int interruptPin) : Sensor::Sensor()
{
  Anemometer::numRevs = 0;
  this->initialize(pin, interruptPin);
}

String Anemometer::toString() //override
{
  char tempString[16];
  dtostrf(getLastSurvey(),2,2,tempString);
  
  return "Vento: " + String(tempString) + " kmh";
}

String Anemometer::getValueName() //override
{
  return "windspeed";
}

void Anemometer::initialize(int pin, int interruptPin) //override
{
  lastReadTime = millis();
  
  pinMode(pin, INPUT);                 //Imposto il pin dell'anemometro come input
  digitalWrite(pin, HIGH);             //Abilito la resistenza di pullup interna
  attachInterrupt(interruptPin, incRevs, FALLING); //Imposto l'incremento dei giri ad ogni interrupt
}

double Anemometer::makeRead() //override
{
  double speedPerSecond = 2.4;        //In km/h, wind speed with one revolution per second
  double revolutions = (double)Anemometer::numRevs;
  Anemometer::numRevs = 0;
  double time = ((double)(millis() - lastReadTime)) / 1000.0;
  lastReadTime = millis();
  
  double speed = revolutions / time * speedPerSecond;
  
  return speed;
}

void Anemometer::incRevs()    //Ho dovuto rendere la funzione statica perchè fosse possibile richiamarla con attachInterrupt()
{
  //Sometimes too many interrupts are fired, so i'm blocking all too fast interrupts (if faster than 50 microsecs)
  //In any case, this allow the system to catch until 200.000 revolutions per second, which is more than enough
  unsigned long now = millis();
  if (now - lastGoodInterrupt > 50) {    
    Anemometer::numRevs++;
  } 
  lastGoodInterrupt = now;
}
