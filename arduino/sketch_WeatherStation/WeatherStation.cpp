#include "WeatherStation.h"
#include <Arduino.h>
#include <ArduinoJson.h>

WeatherStation::WeatherStation(int maxNumOfSensors)
{
  this->sensors = new Sensor*[maxNumOfSensors];
  this->maxNumOfSensors = maxNumOfSensors;
  
  this->valuesStillNotSent = new double[maxNumOfSensors];
  this->valuesReadCounter = new int[maxNumOfSensors];
  
  //Initializing
  this->nextPrintPointer = 0;
  for (int i = 0; i < maxNumOfSensors; i++) {
    sensors[i] = NULL;
  }
}

void WeatherStation::setLcd(LiquidCrystal* lcd)
{
  this->lcd = lcd;
}

void WeatherStation::addSensor(Sensor* sensor)
{
  bool founded = false; 
  for (int i = 0; i < this->maxNumOfSensors && !founded; i++) {
    if (this->sensors[i] == NULL) {
      this->sensors[i] = sensor;
      this->valuesReadCounter[i] = 0;
      founded = true;
    }
  }
}

//Legge i valori e li stampa a schermo
void WeatherStation::refreshLcd()
{
  double* values = this->readAllSensors();
  delete[] values;
  this->printLastReadValuesOnLcd();
}

void WeatherStation::sendValuesOnSerial()
{
  StaticJsonBuffer<200> jsonBuffer;  //Buffer della libreria ArduinoJson
  
  //Costruzione dell'oggetto JSON con tutti i valori dei sensori letti fin'ora
  JsonObject& jsonObj = jsonBuffer.createObject();
  char valueNames[this->maxNumOfSensors][20];
  for (int i = 0; i < this->maxNumOfSensors; i++) {
    if (this->sensors[i]) {
      String __valueName = this->sensors[i]->getValueName();
      __valueName.toCharArray(valueNames[i], 20);
      
      //Inserting ("valueName":avgValue) in json object 
      jsonObj[valueNames[i]] = valuesStillNotSent[i];
      
      //Resetting stored avg values
      this->valuesStillNotSent[i] = 0.0;
      this->valuesReadCounter[i] = 0;
    }
  }
  
  //Stampa sull'interfaccia seriale
  jsonObj.printTo(Serial);
  Serial.println();
  
  //result example: {"temperature":28.12, "brilliance":100.00, "windspeed":3.60, "rainfall":0.00}
}

void WeatherStation::printLastReadValuesOnLcd()
{
  this->lcd->clear();            //Pulisco l'lcd e sposto il cursore sulla prima riga (riga 0) e sulla prima colonna
  
  if (this->nextPrintPointer >= this->maxNumOfSensors) {
    this->nextPrintPointer = 0;
  }
  
  for (int i = 0; i < 2; i++) {
    if (this->nextPrintPointer < this->maxNumOfSensors && this->sensors[this->nextPrintPointer] != NULL) {
      this->lcd->setCursor(0,i);     //Sposto il cursore sulla prima colonna e sulla prima/seconds riga
      this->lcd->print((this->sensors[this->nextPrintPointer])->toString());
    }
    this->nextPrintPointer++;
  }
}

//Ritorna un puntatore all'array di valori letti
double* WeatherStation::readAllSensors()
{
  double* values = new double[this->maxNumOfSensors];
  
  for (int i = 0; i < this->maxNumOfSensors; i++) {
    if (this->sensors[i] != NULL) {
      double value = this->sensors[i]->read();
      values[i] = value;
      
      //Recalcuating averages
      double oldValue = this->valuesStillNotSent[i];
      int oldCounter = this->valuesReadCounter[i];
      this->valuesReadCounter[i]++;
      if (this->sensors[i]->getValueType() == Sensor::VALUE_SUM) {
        this->valuesStillNotSent[i] = oldValue + value;
      } else if (this->sensors[i]->getValueType() == Sensor::VALUE_AVERAGE) {
        this->valuesStillNotSent[i] = ((oldValue*oldCounter) + value) / this->valuesReadCounter[i];
      }
    }
  }
  
  return values;
}
