#ifndef __ANEMOMETER__
#define __ANEMOMETER__

#include "Sensor.h"

class Anemometer : public Sensor
{
    unsigned long lastReadTime;
    volatile static unsigned int numRevs;
    volatile static unsigned long lastGoodInterrupt;
    
public:
    Anemometer(int pin, int interruptPin);

    String toString(); //override
	String getValueName(); //override

protected:
    void initialize(int pin, int interruptPin); //override
    
    double makeRead(); //override
    
private:
    static void incRevs();    //Ho dovuto rendere la funzione statica perchè fosse possibile richiamarla con attachInterrupt()
};

#endif

