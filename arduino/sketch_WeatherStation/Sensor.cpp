#include "Sensor.h"

Sensor::Sensor() {
   this->lastSurvey = 0.0;
}

double Sensor::read()
{
  setLastSurvey(makeRead());
  return getLastSurvey();
}

double Sensor::getLastSurvey()
{
  return this->lastSurvey;
}

void Sensor::setLastSurvey(double value)
{
  this->lastSurvey = value;
}

int Sensor::getValueType()
{
  return Sensor::VALUE_AVERAGE;
}
