#include "RainGauge.h"

#include <Arduino.h>

volatile int RainGauge::numClocks = 0;
volatile unsigned long RainGauge::lastGoodInterrupt = 0;

RainGauge::RainGauge(int pin, int interruptPin) : Sensor::Sensor()
{
  RainGauge::numClocks = 0;
  this->initialize(pin, interruptPin);
}

String RainGauge::toString() //override
{
  char tempString[16];
  dtostrf(getLastSurvey(),2,2,tempString);
  
  return "Piogg: " + String(tempString) + " mm";
}

String RainGauge::getValueName() //override
{
  return "rainfall";
}

void RainGauge::initialize(int pin, int interruptPin) //override
{  
  pinMode(pin, INPUT);                 //Imposto il pin dell'anemometro come input
  digitalWrite(pin, HIGH);             //Abilito la resistenza di pullup interna
  attachInterrupt(interruptPin, incClocks, FALLING); //Imposto l'incremento dei giri ad ogni interrupt
}

double RainGauge::makeRead() //override
{
  double mmPerClock = 0.2794;        //In km/h, wind speed with one revolution per second
  double clocks = (double)RainGauge::numClocks;
  RainGauge::numClocks = 0;
  
  double rain = clocks * mmPerClock;
  
  return rain;
}

void RainGauge::incClocks()    //Ho dovuto rendere la funzione statica perchè fosse possibile richiamarla con attachInterrupt()
{
  unsigned long now = micros();
  if (now - lastGoodInterrupt > 50) {    
    RainGauge::numClocks++;
  } 
  lastGoodInterrupt = now;
}

int RainGauge::getValueType()
{
  return Sensor::VALUE_SUM;
}
