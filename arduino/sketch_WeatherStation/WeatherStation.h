#ifndef __WEATHERSTATION__
#define __WEATHERSTATION__

#include "Sensor.h"
#include <LiquidCrystal.h>

class WeatherStation
{
    LiquidCrystal* lcd;
    Sensor** sensors;
    int maxNumOfSensors;
    int nextPrintPointer;
    
    double* valuesStillNotSent;
    int* valuesReadCounter;
    
public:
    WeatherStation(int maxNumOfSensors);
    void setLcd(LiquidCrystal* lcd);
    void addSensor(Sensor* sensor);
    void refreshLcd();
    void sendValuesOnSerial();
    
    double* readAllSensors();
private:
    void printLastReadValuesOnLcd();
};

#endif 
