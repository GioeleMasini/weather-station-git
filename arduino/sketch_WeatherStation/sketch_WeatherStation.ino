#include <LiquidCrystal.h>   //Libreria per pilotare il display LCD
#include <ArduinoJson.h>     //Libreria per gestire oggetti JSON
#include <TimerOne.h>        //Libreria per la gestione del Timer1 di Arduino

#include "WeatherStation.h"

#include "Anemometer.h"
#include "TemperatureSensor.h"
#include "Photoresistor.h"
#include "RainGauge.h"

#define PINS_LCD           10, 9, 8, 7, 6, 5    //Pin di collegamento dell'lcd
#define PIN_TEMPERATURE    A0     //Pin di collegamento del piedino Vout del sensore di temperatura
#define PIN_PHOTORESISTOR  A1     //Pin di collegamento della fotoresistenza
#define PIN_ANEMOMETER     2      //Pin di collegamento dell'anemometro
#define PIN_RAINGAUGE      3      //Pin di collegamento del pluviometro

#define PIN_INTERRUPT_ANEMOMETER  0   //Pin di interrupt dell'anemometro
#define PIN_INTERRUPT_RAINGAUGE   1   //Pin di interrupt del pluviometro

#define NUM_SENSORS 4       //Slot disponibili nella WeatherStation per l'inserimento di sensori.

#define READ_CYCLES 10      //Cicli di lettura per temperatura e luce, più è alto e più migliora il valore finale
#define LCD_INTERVAL 3      //Intervallo di refresh dell'lcd, in secondi
#define SEND_INTERVAL 60    //Intervallo di invio dei dati sull'interfaccia seriale, in secondi. Deve essere un multiplo di LCD_INTERVAL per essere eseguito con il tempismo corretto.

unsigned int loopCounter;
volatile boolean timerFlag;

//Istanziazione degli oggetti
WeatherStation* station = new WeatherStation(NUM_SENSORS);

LiquidCrystal* lcd = new LiquidCrystal(PINS_LCD); //Inizializzazione dell'lcd con i pin relativi

Anemometer* anemometer = new Anemometer(PIN_ANEMOMETER, PIN_INTERRUPT_ANEMOMETER);
RainGauge* raingauge = new RainGauge(PIN_RAINGAUGE, PIN_INTERRUPT_RAINGAUGE);
TemperatureSensor* tempSensor = new TemperatureSensor(PIN_TEMPERATURE, READ_CYCLES);
Photoresistor* photoresistor = new Photoresistor(PIN_PHOTORESISTOR, READ_CYCLES);

void setup()
{
  Serial.begin(9600);
  
  lcd->begin(16, 2);       //Impostazione del numero di colonne e righe del display LCD 
  station->setLcd(lcd);
  station->addSensor(tempSensor);
  station->addSensor(photoresistor);
  station->addSensor(anemometer);
  station->addSensor(raingauge);
  
  loopCounter = 0;
  timerFlag = true;
  
  analogReference(INTERNAL);
  
  Timer1.initialize(LCD_INTERVAL * 1000000); // set a timer of length 10^6 micro sec = 1 sec	
  Timer1.attachInterrupt(executeLoop);       // execute a loop (reading and eventually sending values on serial)
}

void loop()
{
  while (!timerFlag){}
  timerFlag = false;
  
  step();
}

void executeLoop() {
  timerFlag = true;
}

void step() {
  loopCounter++;
  station->refreshLcd();
  
  if (loopCounter >= SEND_INTERVAL / LCD_INTERVAL) {
    loopCounter = 0;
    station->sendValuesOnSerial();
  } 
}
