#ifndef __SENSOR__
#define __SENSOR__

#include <WString.h>

class Sensor
{      
public:
    //Tokens to specify if more values of this sensor have to be sum
    //or it is necessary to calculate the average between them, to have a correct value
    static const int VALUE_AVERAGE = 1;
    static const int VALUE_SUM = 2;
    
    Sensor();
    
    virtual String toString() = 0;
    virtual String getValueName() = 0;
   
    double read();
    
    double getLastSurvey();
    static int getValueType();
    
protected:
    virtual double makeRead() = 0;
    void setLastSurvey(double value);
    
private:
    volatile double lastSurvey;
};      

#endif
