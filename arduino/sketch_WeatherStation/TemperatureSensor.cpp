#include "TemperatureSensor.h"

#include <Arduino.h>

TemperatureSensor::TemperatureSensor(int pin, int readCycles=10) : Sensor::Sensor()
{
  this->pin = pin;
  this->readCycles = readCycles;
}

String TemperatureSensor::toString() //override
{
  char tempString[16];
  dtostrf(getLastSurvey(),2,2,tempString);
    
  return "Temp: " + String(tempString) + " C";
}

String TemperatureSensor::getValueName() //override
{
  return "temperature";
}

double TemperatureSensor::makeRead() //override 
{
  double temp = 0.0;
  
  for(int i=0; i<10; i++) {               //Esegue più volte le letture
    temp += (analogRead(this->pin)  / 9.31);   //Calcola la temperatura e la somma alla variabile 'temp'
  }
  temp = temp / (float)10.0;     //Calcola la media matematica dei valori di temperatura
  return temp;
}
