#include "Photoresistor.h"

#include <Arduino.h>

Photoresistor::Photoresistor(int pin, int readCycles=10) : Sensor::Sensor()
{
  this->pin = pin;
  this->readCycles = readCycles;
}

String Photoresistor::toString() //override
{
  char tempString[16];
  dtostrf(getLastSurvey(),2,2,tempString);

  return "Luce: " + String(tempString) + " %";
}

String Photoresistor::getValueName() //override
{
  return "brilliance";
}

double Photoresistor::makeRead() //override 
{
  int photoTemp = 0;
  double photo;
  
  for(int i=0; i<this->readCycles; i++) {               //Esegue più volte le letture
    photoTemp += (int)analogRead(this->pin);     //Leggo il valore della fotoresistenza
  }
  photo = (double)photoTemp / (double)this->readCycles;   //Calcolo la media matematica dei valori della fotoresistenza
  photo = photo / 1023.0 * 100.0;                   //Trasformo il valore in percentuale
  
  return photo;
}
 
