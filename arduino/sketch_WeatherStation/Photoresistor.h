#ifndef __PHOTORESISTOR__
#define __PHOTORESISTOR__

#include "Sensor.h"

class Photoresistor : public Sensor
{
    int pin, readCycles;
    
public:
    Photoresistor(int pin);
    Photoresistor(int pin, int numReadsAtTime);

    String toString(); //override
	String getValueName(); //override

protected:
    void initialize(int pin); //override
    
    double makeRead(); //override
    
};

#endif 
